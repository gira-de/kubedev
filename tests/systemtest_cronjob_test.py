import copy
import unittest
from subprocess import CalledProcessError

import yaml
from kubedev import Kubedev
from kubedev.utils import KubernetesTools
from kubedev.utils.errors import CouldNotPullImageInCIException
from test_utils import (EnvMock, FileMock, ShellExecutorMock, SleepMock,
                        TagGeneratorMock, test_cron_job_config_with_deployment,
                        testCronJobConfig)


class KubeDevSystemTestCronJobTests(unittest.TestCase):
    def test_systemtest_spins_up_kind_cluster(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            None,
            False,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertIn([
            "kind",
            "create",
            "cluster",
            "--kubeconfig",
            ".kubedev/kind_config_foo-service-asdf",
            "--wait",
            "10m",
            "--name",
            "kind-foo-service-asdf"
        ], calls)

    def test_systemtest_does_not_spin_up_kind_cluster_if_reusing_existing_cluster(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['reuse-cluster\n', 'postgres-id', 'run-testapi-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            "reuse-cluster",
            False,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertNotIn([
            "kind",
            "create",
            "cluster",
            "--kubeconfig",
            ".kubedev/kind_config_foo-service-asdf",
            "--wait",
            "10m",
            "--name",
            "kind-foo-service-asdf"
        ], calls)
        self.assertNotIn([
            "kind",
            "create",
            "cluster",
            "--kubeconfig",
            ".kubedev/kind_config_foo-service-asdf",
            "--wait",
            "10m",
            "--name",
            "reuse-cluster"
        ], calls)

    def test_systemtest_does_not_delete_cluster_and_network_when_reusing(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['reuse-cluster\n', 'postgres-id', 'run-cronjob-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            "reuse-cluster",
            False,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertNotIn([
            "kind",
            "delete",
            "cluster",
            "--name",
            "reuse-cluster"
        ], calls)
        self.assertNotIn([
            "docker",
            "network",
            "rm",
            "reuse-cluster"
        ], calls)

    def test_systemtest_deletes_kind_cluster(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            None,
            False,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertIn([
            "kind",
            "delete",
            "cluster",
            "--name",
            "kind-foo-service-asdf"
        ], calls)

    def test_systemtest_doesnt_pull_service_image_outside_ci(self):
        # ARRANGE
        file_mock = FileMock()
        env_mock = EnvMock()
        shell_mock = ShellExecutorMock(cmd_output=['postgres-id', 'run-testapi-id', '{"status": {"availableReplicas": 1}}'])
        tag_mock = TagGeneratorMock(["asdf"])
        sleep_mock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            test_cron_job_config_with_deployment,
            "foo-job",
            None,
            True,
            file_accessor=file_mock,
            env_accessor=env_mock,
            shell_executor=shell_mock,
            tag_generator=tag_mock,
            sleeper=sleep_mock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shell_mock._calls]
        self.assertNotIn([
            "docker", "pull", "foo-registry/foo-service-foo-deploy:none"
        ], calls)
        self.assertNotIn([
            "docker", "pull", "postgres:13"
        ], calls)

    def test_systemtest_pulls_service_and_deployment_images_in_ci(self):
        # ARRANGE
        file_mock = FileMock()
        env_mock = EnvMock()
        env_mock.setenv('CI', 'abc')
        shell_mock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tag_mock = TagGeneratorMock(["asdf"])
        sleep_mock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            test_cron_job_config_with_deployment,
            "foo-job",
            None,
            False,
            file_accessor=file_mock,
            env_accessor=env_mock,
            shell_executor=shell_mock,
            tag_generator=tag_mock,
            sleeper=sleep_mock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shell_mock._calls]
        self.assertIn([
            "docker", "pull", "foo-registry/foo-service-foo-deploy:none"
        ], calls)
        self.assertIn([
            "docker", "pull", "postgres:13"
        ], calls)

    def test_systemtest_raises_CouldNotPullImageInCIException_when_pull_service_image_fails_in_ci(self):
        def raiseWhenPullInCmd(cmdWithArgs: list) -> int:
            if 'pull' in cmdWithArgs:
                raise CalledProcessError(1, ' '.join(cmdWithArgs))
            else:
                return 0

        # ARRANGE
        file_mock = FileMock()
        env_mock = EnvMock()
        env_mock.setenv('CI', 'abc')
        shell_mock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api', '{"status": {"availableReplicas": 1}}'],
            execute_handler=lambda cmdWithArgs, _: raiseWhenPullInCmd(cmdWithArgs))
        tag_mock = TagGeneratorMock(["asdf"])
        sleep_mock = SleepMock()

        # ACT
        sut = Kubedev()
        self.assertRaises(CouldNotPullImageInCIException, lambda: sut.system_test_from_config(
            test_cron_job_config_with_deployment,
            "foo-job",
            None,
            False,
            file_accessor=file_mock,
            env_accessor=env_mock,
            shell_executor=shell_mock,
            tag_generator=tag_mock,
            sleeper=sleep_mock))

    def test_systemtest_builds_test_container(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(cmd_output=['postgres-test-id', 'kubedev-run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            None,
            True,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "build",
                "-t",
                "local-foo-job-system-tests-asdf",
                "--build-args",
                'FOO_JOB_BUILD_ARG="asdf"',
                "./systemTests/foo-job/"
            ])
        ], calls)

    def test_systemtest_creates_docker_secret(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        envMock.setenv('DOCKER_AUTH_CONFIG', '{"test": true"}')
        shellMock = ShellExecutorMock(
            cmd_output=['x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            None,
            True,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "run",
                "-i",
                "--rm",
                '--network',
                'local-foo-job-system-tests-asdf',
                '--user',
                '0',
                '--volume',
                "/kubedev/systemtests/.kubedev/kind_config_foo-service-asdf:/tmp/kube_config",
                "--env",
                "DOCKER_AUTH_CONFIG",
                "bitnami/kubectl:1.18",
                "--kubeconfig",
                "/tmp/kube_config",
                "create",
                "secret",
                "generic",
                "foo-creds",
                "--type",
                "kubernetes.io/dockerconfigjson",
                '--from-literal=.dockerconfigjson="${DOCKER_AUTH_CONFIG}"'
            ])], calls)

    def test_systemtest_does_not_create_docker_secret_when_reusing(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        envMock.setenv('DOCKER_AUTH_CONFIG', '{"test": true"}')
        shellMock = ShellExecutorMock(
            cmd_output=['reuse-cluster', 'x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            "reuse-cluster",
            True,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertNotIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "run",
                "-i",
                "--rm",
                '--network',
                'local-foo-job-system-tests-asdf',
                '--user',
                '0',
                '--volume',
                "/kubedev/systemtests/.kubedev/kind_config_foo-service-asdf:/tmp/kube_config",
                "--env",
                "DOCKER_AUTH_CONFIG",
                "bitnami/kubectl:1.18",
                "--kubeconfig",
                "/tmp/kube_config",
                "create",
                "secret",
                "generic",
                "foo-creds",
                "--type",
                "kubernetes.io/dockerconfigjson",
                '--from-literal=.dockerconfigjson="${DOCKER_AUTH_CONFIG}"'
            ])], calls)

    def test_systemtest_builds_apps(self):
        # ARRANGE
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(cmd_output=['postgres-test-id', 'kubedev-run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(["asdf"])
        sleepMock = SleepMock()

        # ACT
        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig,
            "foo-job",
            None,
            True,
            file_accessor=fileMock,
            env_accessor=envMock,
            shell_executor=shellMock,
            tag_generator=tagMock,
            sleeper=sleepMock)

        # ASSERT
        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "build",
                "-t",
                'foo-registry/foo-service-foo-job:asdf',
                '--build-arg',
                'FOO_SERVICE_GLOBAL_ENV1="${FOO_SERVICE_GLOBAL_ENV1}"',
                '--build-arg',
                'FOO_SERVICE_GLOBAL_ENV2="${FOO_SERVICE_GLOBAL_ENV2}"',
                '--build-arg',
                'FOO_SERVICE_JOB_ENV1="${FOO_SERVICE_JOB_ENV1}"',
                '--build-arg',
                'FOO_SERVICE_JOB_ENV2="${FOO_SERVICE_JOB_ENV2}"',
                '--build-arg',
                'FOO_SERVICE_JOB_ENV3="${FOO_SERVICE_JOB_ENV3}"',
                './foo-job/'
            ])
        ], calls)

    def test_systemtest_runs_test_container_with_kubeconf(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd', 'apikey'])
        sleeper = SleepMock()

        sut = Kubedev()
        result = sut.system_test_from_config(
            testCronJobConfig, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)
        # Check that the temporary kube config has group-read access:
        self.assertIn('.kubedev/kind_config_foo-service-abcd',
                      fileMock.chmodPaths)
        # Check for running of the test container:
        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "run",
                "--rm",
                "--network", 'local-foo-job-system-tests-abcd',
                "--name", "foo-job-system-tests-abcd",
                "--interactive",
                "--volume",
                "/kubedev/systemtests/.kubedev/kind_config_foo-service-abcd:/tmp/kube_config",
                "--env",
                "KUBECONFIG=/tmp/kube_config",
                "--env",
                'FOO_SERVICE_GLOBAL_ENV1="${FOO_SERVICE_GLOBAL_ENV1}"',
                "--env",
                'FOO_SERVICE_JOB_ENV1="${FOO_SERVICE_JOB_ENV1}"',
                "--env",
                'FOO_SERVICE_JOB_ENV2="${FOO_SERVICE_JOB_ENV2}"',
                "--env",
                'POSTGRES_USER="testadmin"',
                "--env",
                'KUBEDEV_SYSTEMTEST_DAEMON_APIKEY="apikey"',
                '--env',
                'KUBEDEV_SYSTEMTEST_DAEMON_ENDPOINT="kubedev-run-cronjob-api:5000"',
                "local-foo-job-system-tests-abcd"
            ])
        ], [call['cmd'] for call in shellMock._calls])

    def test_systemtest_runs_test_container_with_local_file_mount(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd', 'apikey'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['cronjobs']['foo-job']['systemTest']['testContainer']['volumes'] = {
            'local_file_short': '/tmp/target_file_short',
            'local_file': {
                'path': '/tmp/target_file',
                'readOnly': True
            }
        }
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)
        # Check for running of the test container:
        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "run",
                "--rm",
                "--network", 'local-foo-job-system-tests-abcd',
                "--name", "foo-job-system-tests-abcd",
                "--interactive",
                "--volume",
                "/kubedev/systemtests/.kubedev/kind_config_foo-service-abcd:/tmp/kube_config",
                "--env",
                "KUBECONFIG=/tmp/kube_config",
                "--volume",
                "/kubedev/systemtests/local_file_short:/tmp/target_file_short",
                "--volume",
                "/kubedev/systemtests/local_file:/tmp/target_file:ro",
                "--env",
                'FOO_SERVICE_GLOBAL_ENV1="${FOO_SERVICE_GLOBAL_ENV1}"',
                "--env",
                'FOO_SERVICE_JOB_ENV1="${FOO_SERVICE_JOB_ENV1}"',
                "--env",
                'FOO_SERVICE_JOB_ENV2="${FOO_SERVICE_JOB_ENV2}"',
                "--env",
                'POSTGRES_USER="testadmin"',
                "--env",
                'KUBEDEV_SYSTEMTEST_DAEMON_APIKEY="apikey"',
                '--env',
                'KUBEDEV_SYSTEMTEST_DAEMON_ENDPOINT="kubedev-run-cronjob-api:5000"',
                "local-foo-job-system-tests-abcd"
            ])
        ], [call['cmd'] for call in shellMock._calls])

    def test_systemtest_run_with_service_from_registry_with_variables(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "create",
                "--network",
                "local-foo-job-system-tests-abcd",
                "--name",
                "postgres-test",
                "--rm",
                "--env",
                'POSTGRES_USER="testadmin"',
                "--env",
                'POSTGRES_PASSWORD="correct horse battery staple"',
                "--publish",
                "5432",
                "postgres:13",
            ])], [call['cmd'] for call in shellMock._calls])

    def test_systemtest_run_with_service_from_registry_with_file_mounts(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['cronjobs']['foo-job']['systemTest']['services']['postgres:13']['volumes'] = {
            'local_file_short': '/tmp/target_file_short',
            'local_file': {
                'path': '/tmp/target_file',
                'readOnly': True
            }
        }
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "create",
                "--network",
                "local-foo-job-system-tests-abcd",
                "--name",
                "postgres-test",
                "--rm",
                "--volume",
                "/kubedev/systemtests/local_file_short:/tmp/target_file_short",
                "--volume",
                "/kubedev/systemtests/local_file:/tmp/target_file:ro",
                "--env",
                'POSTGRES_USER="testadmin"',
                "--env",
                'POSTGRES_PASSWORD="correct horse battery staple"',
                "--publish",
                "5432",
                "postgres:13",
            ])], [call['cmd'] for call in shellMock._calls])

    def test_systemtest_changes_kubeconf_servers_to_kind_control_plane(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        fileMock.save_file('.kubedev/kind_config_foo-service-abcd', '''apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: Foo
    server: https://127.0.0.1:58436
  name: kind-kind-foo-service-abcd
contexts:
- context:
    cluster: kind-kind-foo-service-abcd
    user: kind-kind-foo-service-abcd
  name: kind-kind-foo-service-abcd
current-context: kind-kind-foo-service-abcd
kind: Config
preferences: {}
users:
- name: kind-kind-foo-service-abcd
  user:
    client-certificate-data: Foo
    client-key-data: Foo
''', overwrite=True, private=False)

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        kubeConfFile = fileMock.load_file(
            '.kubedev/kind_config_foo-service-abcd')
        self.assertIsNotNone(kubeConfFile)
        kubeConf = yaml.safe_load(kubeConfFile)
        for cluster in kubeConf['clusters']:
            self.assertEqual(
                f'https://kind-foo-service-abcd-control-plane:6443', cluster['cluster']['server'])

    def test_systemtest_succeeds_without_env_vars(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        del config['required-envs']
        del config['cronjobs']['foo-job']['required-envs']
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

    def test_systemtest_deletes_docker_network(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['x', 'y', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertIn([
            'docker',
            'network',
            'rm',
            'local-foo-job-system-tests-abcd'
        ], [call['cmd'] for call in shellMock._calls])

    def test_sytemtest_passes_args_to_services(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['cronjobs']['foo-job']['systemTest']['services']['postgres:13']['cmd'] = [
            'fancy', 'arguments', '${SHELLVAR}']
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                "docker",
                "create",
                "--network",
                "local-foo-job-system-tests-abcd",
                "--name",
                "postgres-test",
                "--rm",
                "--env",
                'POSTGRES_USER="testadmin"',
                "--env",
                'POSTGRES_PASSWORD="correct horse battery staple"',
                "--publish",
                "5432",
                "postgres:13",
                "fancy",
                "arguments",
                "${SHELLVAR}"
            ])], [call['cmd'] for call in shellMock._calls])

    def test_sytemtest_overwrites_required_envs_with_variables_when_deploying(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['cronjobs']['foo-job']['systemTest']['variables'] = {
            'FOO_SERVICE_GLOBAL_ENV1': 'overwritten!',
            'FOO_SERVICE_JOB_ENV2': '${FOO_SERVICE_JOB_ENV2_OVERWRITE}'
        }
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                'docker',
                'run',
                '-i',
                '--rm',
                '--network',
                'local-foo-job-system-tests-abcd',
                '--user',
                '0',
                '--volume',
                '/kubedev/systemtests/.kubedev/kind_config_foo-service-abcd:/tmp/kube_config',
                '--volume',
                '/kubedev/systemtests/helm-chart/:/app/helm-chart/',
                'alpine/helm:3.8.0',
                'upgrade',
                'foo-service',
                '/app/helm-chart/',
                '--install',
                '--wait',
                '--kubeconfig',
                '/tmp/kube_config',
                '--set', 'KUBEDEV_TAG=abcd',
                '--set', 'FOO_SERVICE_GLOBAL_ENV1="overwritten!"',
                '--set', 'FOO_SERVICE_JOB_ENV1="${FOO_SERVICE_JOB_ENV1}"',
                '--set', 'FOO_SERVICE_JOB_ENV2="${FOO_SERVICE_JOB_ENV2_OVERWRITE}"'
            ])], [call['cmd'] for call in shellMock._calls])

    def test_sytemtest_deploys_cluster_prepare_chart_before_this_service(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['cronjobs']['foo-job']['systemTest']['variables'] = {
            'FOO_SERVICE_GLOBAL_ENV1': 'overwritten!',
            'FOO_SERVICE_JOB_ENV2': '${FOO_SERVICE_JOB_ENV2_OVERWRITE}',
            'SYSTEMTEST_NEW_ENV': "hello!"
        }
        config['cronjobs']['foo-job']['systemTest']['clusterInitChart'] = 'systemTests/cluster-prepare-chart/'
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                'docker',
                'run',
                '-i',
                '--rm',
                '--network',
                'local-foo-job-system-tests-abcd',
                '--user',
                '0',
                '--volume',
                '/kubedev/systemtests/.kubedev/kind_config_foo-service-abcd:/tmp/kube_config',
                '--volume',
                '/kubedev/systemtests/systemTests/cluster-prepare-chart/:/app/helm-chart/',
                'alpine/helm:3.8.0',
                'upgrade',
                'prepare-foo-service',
                '/app/helm-chart/',
                '--install',
                '--wait',
                '--kubeconfig',
                '/tmp/kube_config',
                '--set', 'KUBEDEV_TAG=abcd',
                '--set', 'FOO_SERVICE_GLOBAL_ENV1="overwritten!"',
                '--set', 'FOO_SERVICE_JOB_ENV1="${FOO_SERVICE_JOB_ENV1}"',
                '--set', 'FOO_SERVICE_JOB_ENV2="${FOO_SERVICE_JOB_ENV2_OVERWRITE}"',
                '--set', 'SYSTEMTEST_NEW_ENV="hello!"'
            ])], [call['cmd'] for call in shellMock._calls])

    def test_sytemtest_does_not_deploy_cluster_prepare_chart_if_not_specified(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['cronjobs']['foo-job']['systemTest']['variables'] = {
            'FOO_SERVICE_GLOBAL_ENV1': 'overwritten!',
            'FOO_SERVICE_JOB_ENV2': '${FOO_SERVICE_JOB_ENV2_OVERWRITE}',
            'SYSTEMTEST_NEW_ENV': "hello!"
        }
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        self.assertNotIn([
            'docker',
            'run',
            '-i',
            '--rm',
            '--network',
            'local-foo-job-system-tests-abcd',
            '--user',
            '0',
            '--volume',
            '/kubedev/systemtests/.kubedev/kind_config_foo-service-abcd:/tmp/kube_config',
            '--volume',
            '/kubedev/systemtests/systemTests/cluster-prepare-chart/:/app/helm-chart/',
            'alpine/helm:3.8.0',
            'upgrade',
            'prepare-foo-service',
            '/app/helm-chart/',
            '--install',
            '--wait',
            '--kubeconfig',
            '/tmp/kube_config',
            '--set', 'KUBEDEV_TAG=abcd',
            '--set', 'FOO_SERVICE_GLOBAL_ENV1="overwritten!"',
            '--set', 'FOO_SERVICE_JOB_ENV1="${FOO_SERVICE_JOB_ENV1}"',
            '--set', 'FOO_SERVICE_JOB_ENV2="${FOO_SERVICE_JOB_ENV2_OVERWRITE}"',
            '--set', 'SYSTEMTEST_NEW_ENV="hello!"'
        ], [call['cmd'] for call in shellMock._calls])

    def test_sytemtest_builds_and_pushes_deployments(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['deployments'] = {
            'foo-deploy': {
                'required-envs': {
                    'FOO_DEPLOY_BUILD': {
                        'documentation': 'x',
                        'container': False,
                        'build': True
                    }
                }
            }
        }
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                'docker',
                'build',
                '-t',
                'foo-registry/foo-service-foo-deploy:abcd',
                '--build-arg',
                'FOO_DEPLOY_BUILD="${FOO_DEPLOY_BUILD}"',
                '--build-arg',
                'FOO_SERVICE_GLOBAL_ENV1="${FOO_SERVICE_GLOBAL_ENV1}"',
                '--build-arg',
                'FOO_SERVICE_GLOBAL_ENV2="${FOO_SERVICE_GLOBAL_ENV2}"',
                './foo-deploy/'
            ])
        ], calls)

        self.assertIn([
            "/bin/sh",
            "-c",
            " ".join([
                'docker',
                'push',
                'foo-registry/foo-service-foo-deploy:abcd'
            ])
        ], calls)

    def test_sytemtest_does_not_build_and_push_generics(self):
        fileMock = FileMock()
        envMock = EnvMock()
        shellMock = ShellExecutorMock(
            cmd_output=['postgres-id', 'run-cronjob-api-id', '{"status": {"availableReplicas": 1}}'])
        tagMock = TagGeneratorMock(['abcd'])
        sleeper = SleepMock()

        sut = Kubedev()
        config = copy.deepcopy(testCronJobConfig)
        config['generic'] = {
            'foo-deploy': {
                'required-envs': {
                    'FOO_DEPLOY_BUILD': {
                        'documentation': 'x',
                        'container': False,
                        'build': True
                    }
                }
            }
        }
        result = sut.system_test_from_config(
            config, 'foo-job', None, True, fileMock, envMock, shellMock, tagMock, sleeper)

        self.assertTrue(result)

        calls = [call['cmd'] for call in shellMock._calls]
        self.assertNotIn([
            "/bin/sh",
            "-c",
            " ".join([
                'docker',
                'build',
                '-t',
                'foo-registry/foo-service-foo-deploy:abcd',
                '--build-arg',
                'FOO_DEPLOY_BUILD="${FOO_DEPLOY_BUILD}"',
                '--build-arg',
                'FOO_SERVICE_GLOBAL_ENV1="${FOO_SERVICE_GLOBAL_ENV1}"',
                '--build-arg',
                'FOO_SERVICE_GLOBAL_ENV2="${FOO_SERVICE_GLOBAL_ENV2}"',
                './foo-deploy/'
            ])
        ], calls)

        self.assertNotIn([
            "/bin/sh",
            "-c",
            " ".join([
                'docker',
                'push',
                'foo-registry/foo-service-foo-deploy:abcd'
            ])
        ], calls)
